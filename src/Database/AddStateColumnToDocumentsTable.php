<?php

namespace Drupal\tmgmt_smartcat\Database;

use Drupal\Core\Database\Schema;

class AddStateColumnToDocumentsTable
{
    private Schema $schema;

    private const TABLE = 'tmgmt_smartcat_documents';

    public function __construct()
    {
        $this->schema = \Drupal::database()->schema();
    }

    public static function run()
    {
        (new self())();
    }

    public function __invoke()
    {
        if ($this->schema->tableExists(self::TABLE) && ! $this->schema->fieldExists(self::TABLE, 'state')) {
            $this->schema->addField(self::TABLE, 'state', [
                'type' => 'varchar',
                'length' => 255,
                'not null' => false,
                'default' => null,
                'description' => 'Smartcat document state',
            ]);
        }
    }
}
