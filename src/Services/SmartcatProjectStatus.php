<?php

namespace Drupal\tmgmt_smartcat\Services;

use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt_smartcat\API\API;
use GuzzleHttp\Exception\ClientException;

class SmartcatProjectStatus
{
    /**
     * @throws TMGMTException
     */
    public function check(JobInterface $job): bool
    {
        $translator = $job->getTranslator();

        $isFailed = false;

        $sc = API::sc($translator);

        try {
            $sc->getProject($job->getReference());

            if ($job->isRejected()) {
                $job->setState($job->isContinuous() ? JobInterface::STATE_CONTINUOUS : JobInterface::STATE_ACTIVE);
            }
        } catch (ClientException $e) {
            switch ($e->getCode()) {
                case 404:
                    $message = "The project was not found in Smartcat: {$job->getReference()}";
                    if ($job->isContinuous()) {
                        $job->setState(JobInterface::STATE_CONTINUOUS_INACTIVE, $message);
                    } else {
                        $job->rejected($message);
                    }
                    break;
                case 403:
                    $message = "Access to the project is denied: {$job->getReference()}. Please, check the credentials and try again.";
                    if ($job->isContinuous()) {
                        $job->setState(JobInterface::STATE_CONTINUOUS_INACTIVE, $message);
                    } else {
                        $job->rejected($message);
                    }
                    break;
                default:
                    $message = "Smartcat project can not be imported: {$job->getReference()}. Please contact support Smartcat team.";
                    if ($job->isContinuous()) {
                        $job->setState(JobInterface::STATE_CONTINUOUS_INACTIVE, $message);
                    } else {
                        $job->rejected($message);
                    }
            }

            $isFailed = true;
        }

        return ! $isFailed;
    }
}
