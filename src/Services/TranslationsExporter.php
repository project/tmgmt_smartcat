<?php

namespace Drupal\tmgmt_smartcat\Services;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\JobItem;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt_smartcat\API\API;
use Drupal\tmgmt_smartcat\API\Clients\IHub;
use Drupal\tmgmt_smartcat\API\Clients\Smartcat;
use Drupal\tmgmt_smartcat\Data\ExportBeginData;
use Drupal\tmgmt_smartcat\Data\ExportedItem;
use GuzzleHttp\Exception\RequestException;

class TranslationsExporter
{
    private Data $data;

    private Smartcat $sc;

    private IHub $ihub;

    private Connection $db;

    private ImmutableConfig $tmgmtConfig;

    private SmartcatProjectStatus $projectStatus;

    public function __construct(Data $data)
    {
        $this->data = $data;

        /** @var Connection $connection */
        $this->db = \Drupal::service('database');

        $this->tmgmtConfig = \Drupal::config('tmgmt.settings');

        $this->projectStatus = \Drupal::service('tmgmt_smartcat.project_status');
    }

    /**
     * @throws TMGMTException
     */
    public function export(JobInterface $job)
    {
        $jobItems = $job->getItems();

        $job = reset($jobItems)->getJob();

        if (! $this->projectStatus->check($job)) {
            return;
        }

        foreach ($jobItems as $jobItem) {
            $this->exportItem($jobItem);
        }
    }

    /**
     * @throws TMGMTException
     */
    public function continuousExport()
    {
        $ids = (new SmartcatDocument())->getReadyToExportJobItemIds($this->limit());

        if (empty($ids)) {
            $this->updateDocumentsProgress();

            return;
        }

        $jobItemsByJob = [];

        foreach (JobItem::loadMultiple($ids) as $item) {
            $jobItemsByJob[$item->getJobId()][] = $item;
        }

        foreach ($jobItemsByJob as $items) {
            foreach ($items as $item) {
                $this->exportItem($item);
            }
        }
    }

    /**
     * @throws TMGMTException
     */
    public function exportItem(JobItemInterface $jobItem)
    {
        $job = $jobItem->getJob();
        $translator = $job->getTranslator();

        if (! $this->projectStatus->check($job)) {
            $jobItem->setState(JobItemInterface::STATE_ABORTED, 'The project is not available in Smartcat');
            (new SmartcatDocument())->deleteByJobItemId($jobItem->id());

            return;
        }

        $this->api($job);

        try {
            $beginData = ExportBeginData::create(
                $jobItem->id(),
                $this->ihub->accountId(),
                $job->getReference(),
                $job->getTargetLangcode()
            );

            $exportBeginResponse = $this->ihub->exportBegin($beginData);

            $exportInfo = $exportBeginResponse->getExportInfo();

            if (! $exportInfo->isFailed()) {
                $attempt = 1;

                while (true) {
                    if ($attempt > 10) {
                        $this->exportingError($job, $jobItem, 'The number of attempts to obtain the translation result has been exceeded (max 10)');
                        break;
                    }

                    $exportResultResponse = $this->ihub->exportResult($exportInfo);

                    if ($exportResultResponse->hasItems()) {
                        $segments = $exportResultResponse->getItems();
                        $translations = $this->mapSegments($segments);
                        $job->addTranslatedData($translations);

                        if ($translator->isAutoAccept()) {
                            $jobItem->acceptTranslation();
                        }

                        (new SmartcatDocument())->deleteByJobItemId($jobItem->id());

                        break;
                    }

                    // sleep(1);

                    $attempt++;
                }
            } else {
                $this->exportingError($job, $jobItem, $exportInfo->getFailureReason());
            }
        } catch (RequestException $e) {
            $this->exportingError($job, $jobItem, $e->getResponse()->getBody()->getContents());
        }
    }

    /**
     * @throws TMGMTException
     */
    private function updateDocumentsProgress()
    {
        $items = $this->getJobsWithProject();

        foreach ($items as $item) {
            $job = Job::load($item['tjid']);

            $this->api($job);

            $project = $this->sc->getProject($item['reference']);

            $project->updateDocumentsProgress();
        }
    }

    private function limit()
    {
        return $this->tmgmtConfig->get('job_items_cron_limit');
    }

    private function getJobsWithProject(): array
    {
        $query = $this->db->select('tmgmt_job', 'p');
        $query->fields('p', ['tjid', 'reference']);
        $query->condition('reference', null, 'IS NOT NULL');
        $query->where("translator LIKE '%smartcat%'");

        $items = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $seen = [];

        return array_filter($items, function ($item) use (&$seen) {
            if (! in_array($item['reference'], $seen)) {
                $seen[] = $item['reference'];

                return true;
            }

            return false;
        });
    }

    /**
     * @param  array<ExportedItem>  $segments
     */
    private function mapSegments(array $segments): array
    {
        $document = [];

        foreach ($segments as $segment) {
            $document[$segment->getId()] = [];
            $document[$segment->getId()]['#text'] = $segment->getTranslation();
        }

        return $this->data->unflatten($document);
    }

    private function exportingError(JobInterface $job, JobItemInterface $jobItem, string $failureReason)
    {
        \Drupal::logger('tmgmt_smartcat')->error('@message | Job ID: @job_id | Job Item ID: @job_item_id | Project ID: @project_id | Failure reason: @failure_reason', [
            '@message' => 'An error occurred while exporting translations from Smartcat',
            '@job_id' => $job->id(),
            '@job_item_id' => $jobItem->id(),
            '@project_id' => $job->getReference(),
            '@failure_reason' => $failureReason,
        ]);
    }

    /**
     * @throws TMGMTException
     */
    private function api(JobInterface $job)
    {
        $translator = $job->getTranslator();

        $this->sc = API::sc($translator);
        $this->ihub = API::ihub($translator);
    }
}
