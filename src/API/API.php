<?php

namespace Drupal\tmgmt_smartcat\API;

use Drupal\tmgmt\Entity\Translator;
use Drupal\tmgmt_smartcat\API\Clients\IHub;
use Drupal\tmgmt_smartcat\API\Clients\Smartcat;

class API
{
    public static function sc(Translator $translator): Smartcat
    {
        $credentials = self::credentials($translator);

        return new Smartcat(
            $credentials['accountId'],
            $credentials['secretKey'],
            $credentials['server']
        );
    }

    public static function ihub(Translator $translator): IHub
    {
        $credentials = self::credentials($translator);

        return new IHub(
            $credentials['accountId'],
            $credentials['secretKey'],
            $credentials['server']
        );
    }

    private static function credentials(Translator $translator): array
    {
        return [
            'accountId' => $translator->getSetting('account_id'),
            'secretKey' => $translator->getSetting('api_key'),
            'server' => $translator->getSetting('server'),
        ];
    }
}
