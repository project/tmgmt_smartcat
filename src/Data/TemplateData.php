<?php

namespace Drupal\tmgmt_smartcat\Data;

class TemplateData
{
    private string $id;

    private string $name;

    /**
     * @var string[]
     */
    private array $sourceLocales;

    /**
     * @var string[]
     */
    private array $targetLocales;

    public function __construct(string $id, string $name, array $source_locales, array $target_locales)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sourceLocales = $source_locales;
        $this->targetLocales = $target_locales;
    }

    public static function createFromArray(array $data): self
    {
        return new self(
            $data['id'],
            $data['name'],
            $data['sourceLocales'],
            $data['targetLocales']
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSourceLocales(): array
    {
        return $this->sourceLocales;
    }

    public function getTargetLocales(): array
    {
        return $this->targetLocales;
    }
}
