<?php

namespace Drupal\tmgmt_smartcat\Data;

class TemplateResponseData
{
    /**
     * @var TemplateData[]
     */
    private array $templates;

    private bool $haveAccessToProjectTemplates;

    /**
     * @param array $templates
     * @param bool $haveAccessToProjectTemplates
     */
    public function __construct(array $templates, bool $haveAccessToProjectTemplates)
    {
        $this->templates = array_map(function ($template) {
            return TemplateData::createFromArray($template);
        }, $templates);

        $this->haveAccessToProjectTemplates = $haveAccessToProjectTemplates;
    }

    public static function createFromArray(array $data): self
    {
        return new self($data['templates'], $data['haveAccessToProjectTemplates']);
    }

    /**
     * @return TemplateData[]
     */
    public function getTemplates(): array
    {
        return $this->templates;
    }

    public function getHaveAccessToProjectTemplates(): bool
    {
        return $this->haveAccessToProjectTemplates;
    }

    public function getOptions(string $sourceLanguageId, string $targetLanguageId): array
    {
        $options = [];

        $filteredTemplates = array_filter($this->templates, function ($template) use ($sourceLanguageId, $targetLanguageId) {
            return in_array($sourceLanguageId, $template->getSourceLocales())
                && in_array($targetLanguageId, $template->getTargetLocales());
        });

        if ($this->templates) {
            foreach ($filteredTemplates as $template) {
                $options[$template->getId()] = $template->getName();
            }
        }

        return $options;
    }
}
