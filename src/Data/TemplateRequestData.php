<?php

namespace Drupal\tmgmt_smartcat\Data;

class TemplateRequestData implements DataInterface
{
    private string $workspaceId;

    public function __construct(string $workspaceId)
    {
        $this->workspaceId = $workspaceId;
    }

    public static function create(string $workspaceId): self
    {
        return new self($workspaceId);
    }

    public function toArray(): array
    {
        return [
            'workspaceId' => $this->workspaceId,
        ];
    }
}
