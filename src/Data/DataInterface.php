<?php

namespace Drupal\tmgmt_smartcat\Data;

interface DataInterface
{
    public function toArray(): array;
}
